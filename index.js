var Plugin = require('origami-plugin');
var Client = require('origami-client');

function CranePluginHandler(api) {
  if (!api) throw new Error('api is required');

  var self = this;

  this.locals = {};

  return function (socket, crane) {
    crane
    .createSocket()
    .then(function (secondary) {
      var instance = new Plugin(
        api,
        function (stackToken) {
          return new Client(secondary, stackToken);
        }
      );

      instance.listenSocket(socket);
    });
  };
}

module.exports = CranePluginHandler;
