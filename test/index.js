var assert = require('assert');
var StubPlugin1 = require('./stub-plugin-1');
var Crane = require('origami-crane');
var CranePluginHandler = require('..');
var EventEmitter2 = require('eventemitter2').EventEmitter2;
var crypto = require('origami-crypto-utils');
var testData = require('./testData');
var stackToken = crypto.encryptAndSign(
  {
    attr1: '1'
  },
  testData.stack.privateKey,
  testData.stack.publicKey
);

describe(
  'CranePluginHandler',
  function () {
    it('requires an api', function () {
      assert.throws(
        function () {
          new CranePluginHandler();
        },
        /api is required/
      )
    });

    it('returns a handler', function () {
      var handler = new CranePluginHandler(StubPlugin1);

      assert(handler1);
    });

    var handler1;

    before(function () {
      handler1 = new CranePluginHandler(StubPlugin1);
    });

    it('asks for a secondary socket', function (done) {
      var socket1 = new EventEmitter2();

      handler1(
        socket1,
        {
          createSocket: function () {
            done();
            return new Promise(function (resolve, reject) {});
          }
        }
      );
    });

    it('emits describe-apis on secondary socket on method invokation', function (done) {
      var socket1 = new EventEmitter2();
      var secondary = new EventEmitter2();

      secondary
      .on(
        'describe-apis',
        function (token, callback) {
          done();
        }
      );

      var resolveSecondary;

      handler1(
        socket1,
        {
          createSocket: function () {
            return Promise.resolve(secondary);
          }
        }
      );

      setTimeout(function () {
        socket1
        .emit('identify', function (kind, pluginName, methods) {
          socket1
          .emit(
            'api',
            stackToken,
            {},
            'echo',
            {
              'what': 'this'
            },
            function (err, result) {

            }
          );
        });
      });
    });

    it('passes token on local clients creation', function (done) {
      var socket1 = new EventEmitter2();
      var secondary = new EventEmitter2();

      secondary
      .on(
        'describe-apis',
        function (token, callback) {
          try {
            assert.deepEqual(token, stackToken);

            done();
          } catch (e) {
            done(e);
          }
        }
      );

      var resolveSecondary;

      handler1(
        socket1,
        {
          createSocket: function () {
            return Promise.resolve(secondary);
          }
        }
      );

      setTimeout(function () {
        socket1
        .emit('identify', function (kind, pluginName, methods) {
          socket1
          .emit(
            'api',
            stackToken,
            {},
            'echo',
            {
              'what': 'this'
            },
            function (err, result) {

            }
          );
        });
      });
    });

    it('uses seconday socket for proxies', function (done) {
      var socket1 = new EventEmitter2();
      var secondary = new EventEmitter2();

      secondary
      .on(
        'describe-apis',
        function (token, callback) {
          callback(null, {
            'OtherPlugin': {
              'do2': []
            }
          })
        }
      );

      secondary
      .on(
        'api',
        function (token, pluginName, methodName, params, callback) {
          done();
        }
      );

      var resolveSecondary;

      handler1(
        socket1,
        {
          createSocket: function () {
            return Promise.resolve(secondary);
          }
        }
      );

      setTimeout(function () {
        socket1
        .emit('identify', function (kind, pluginName, methods) {
          socket1
          .emit(
            'api',
            stackToken,
            {},
            'invokeOtherPlugin',
            {
            },
            function (err, result) {
            }
          );
        });
      });
    });
  }
);
