function StubPlugin1 (OtherPlugin) {
  this.OtherPlugin = OtherPlugin;
}

StubPlugin1.prototype.echo = function (what) {
  return Promise.resolve(what);
};

StubPlugin1.prototype.invokeOtherPlugin = function () {
  return this.OtherPlugin.do2();
};

module.exports = StubPlugin1;
